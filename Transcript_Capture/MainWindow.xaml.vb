﻿Imports System.Data
Imports System.Text.RegularExpressions


Namespace Transcript_Capture
    Partial Public Class MainWindow
        Inherits Window

        Private _noOfErrorsOnScreen As Integer = 0
        Private _Validation As New clsValidation()
        Dim dtGPA_Conversion As dsGPA_Conversion.EM_WD_GetGPA_ConversionDataTable

        Dim EM_WD_Users_ID As Integer ' The ID of the User populated on form load
        Dim EM_WD_Team_ID As Integer ' The ID of the User's team populated on form load

        Dim dtImages As DataTable

        Dim GPA_Rounds(3, 3) As String
        Dim GPA_Round As Integer
        Dim Used_GPA_Rounds_User_IDs As String
        Dim GPA_Passed_QC As Boolean

        Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
            If Not Form_Active() Then
                btnSubmit.Visibility = Windows.Visibility.Hidden
                MessageBox.Show("The current form is disabled or you are using an older version.")
            End If

            Setup_User_ID()

            'If no user is found close the form
            If EM_WD_Users_ID = 0 Then
                'Close the form
                Me.Close()
                Exit Sub
            End If

            Dim intLoopIndex As Integer
            For intLoopIndex = 0 To 99

                Dim txtGrade As TextBox = New TextBox()
                txtGrade.Height = "23"
                txtGrade.HorizontalAlignment = Windows.HorizontalAlignment.Left
                txtGrade.Margin = New Thickness(39, (intLoopIndex * 52) + 5, 0, 0)
                txtGrade.Name = "txtGrade_" & intLoopIndex.ToString("D2")
                txtGrade.VerticalAlignment = Windows.VerticalAlignment.Top
                txtGrade.Width = 57
                txtGrade.MaxLength = 6
                AddHandler txtGrade.LostFocus, AddressOf txtGrade_Lost_Focus

                Grid1.Children.Add(txtGrade)

                Dim rdlCP As RadioButton = New RadioButton()
                rdlCP.Height = "16"
                rdlCP.Content = "CP"
                rdlCP.FontSize = "10"
                rdlCP.HorizontalAlignment = Windows.HorizontalAlignment.Left
                rdlCP.Margin = New Thickness(109, (intLoopIndex * 52) + 5, 0, 0)
                rdlCP.Name = "rdlCP_" & intLoopIndex.ToString("D2")
                rdlCP.VerticalAlignment = Windows.VerticalAlignment.Top
                rdlCP.GroupName = "Type_" & intLoopIndex
                rdlCP.Width = 57
                rdlCP.IsChecked = True

                Grid1.Children.Add(rdlCP)

                Dim rdlHon As RadioButton = New RadioButton()
                rdlHon.Height = "16"
                rdlHon.Content = "Honors"
                rdlHon.FontSize = "10"
                rdlHon.HorizontalAlignment = Windows.HorizontalAlignment.Left
                rdlHon.Margin = New Thickness(109, (intLoopIndex * 52) + 20, 0, 0)
                rdlHon.Name = "rdlHon_" & intLoopIndex.ToString("D2")
                rdlHon.VerticalAlignment = Windows.VerticalAlignment.Top
                rdlHon.GroupName = "Type_" & intLoopIndex
                rdlHon.Width = 57
                rdlHon.IsTabStop = True

                Grid1.Children.Add(rdlHon)

                Dim rdlAP As RadioButton = New RadioButton()
                rdlAP.Height = "16"
                rdlAP.Content = "AP/IB"
                rdlAP.FontSize = "10"
                rdlAP.HorizontalAlignment = Windows.HorizontalAlignment.Left
                rdlAP.Margin = New Thickness(109, (intLoopIndex * 52) + 35, 0, 0)
                rdlAP.Name = "rdlAP_" & intLoopIndex.ToString("D2")
                rdlAP.VerticalAlignment = Windows.VerticalAlignment.Top
                rdlAP.GroupName = "Type_" & intLoopIndex
                rdlAP.Width = 57
                rdlAP.IsTabStop = True

                Grid1.Children.Add(rdlAP)

                Dim txtCredit As TextBox = New TextBox()
                txtCredit.Height = "23"
                txtCredit.HorizontalAlignment = Windows.HorizontalAlignment.Left
                txtCredit.Margin = New Thickness(163, (intLoopIndex * 52) + 5, 0, 0)
                txtCredit.Name = "txtCredit_" & intLoopIndex.ToString("D2")
                txtCredit.VerticalAlignment = Windows.VerticalAlignment.Top
                txtCredit.Width = 57
                txtCredit.MaxLength = 6

                Grid1.Children.Add(txtCredit)


                Dim Row As Label = New Label()
                Row.Height = "23"
                Row.HorizontalAlignment = Windows.HorizontalAlignment.Left
                Row.Margin = New Thickness(7, (intLoopIndex * 52) + 5, 0, 0)
                Row.Name = "Row_" & intLoopIndex.ToString("D2")
                Row.Content = intLoopIndex + 1
                Row.VerticalAlignment = Windows.VerticalAlignment.Top

                Grid1.Children.Add(Row)

                'Load GPA conversion table
                Load_GPA_Conversion()

                grid.DataContext = _Validation
                Grid1.DataContext = _Validation
            Next

            'Output the version to a label on the form
            lblVersion.Content = Application.Current.MainWindow.GetType().Assembly.GetName().Version.ToString

        End Sub

        Private Function Form_Active() As Boolean
            Dim bStatus As Boolean = False

            'Get the form version from the database
            Dim dtForm_Check As DataTable
            Dim TA_Form_Check As New dsCheck_Form_StatusTableAdapters.EM_WD_Check_Form_StatusTableAdapter
            dtForm_Check = TA_Form_Check.GetData_Form_Status(Application.Current.MainWindow.GetType().Assembly.GetName().Name, _
                                                             Application.Current.MainWindow.GetType().Assembly.GetName().Version.ToString)

            If dtForm_Check.Rows.Count = 0 Then
                'There are no forms with the current version.  
                bStatus = False
            Else
                bStatus = dtForm_Check.Rows(0).Item(0).ToString
            End If

            Return bStatus

        End Function

        Private Sub Validation_Error(sender As Object, e As ValidationErrorEventArgs)
            If e.Action = ValidationErrorEventAction.Added Then
                _noOfErrorsOnScreen += 1
            Else
                _noOfErrorsOnScreen -= 1
            End If
        End Sub

        Protected Sub dgImages_Selectionchanged(sender As Object, e As SelectionChangedEventArgs)
            If dgImages.SelectedIndex > -1 Then
                Dim Image_ID As Integer = DirectCast(dgImages.SelectedItem, DataRowView).Row("Image_ID").ToString()
                Dim Image_Filename As String = DirectCast(dgImages.SelectedItem, DataRowView).Row("Image_Filename").ToString()
                Dim Image_Full_Path As String = DirectCast(dgImages.SelectedItem, DataRowView).Row("Image_Full_Path").ToString()

                txtImage_ID_Banner_ID.Text = Image_ID

                'Clear the array
                ReDim GPA_Rounds(4, 4)

                'Start the GPA at round 1
                GPA_Round = 1

                Try
                    'Setup the User ID
                    Dim dtGPA As DataTable
                    Dim TA_GPA As New dsGPATableAdapters.EM_WD_GetImage_GPAsTableAdapter
                    dtGPA = TA_GPA.GetData_GPA(Image_ID)
                    If dtGPA.Rows.Count > 0 Then
                        GPA_Rounds(0, 0) = dtGPA.Rows(0).Item(1).ToString ' GPA_Round_1
                        GPA_Rounds(0, 1) = dtGPA.Rows(0).Item(2).ToString ' GPA_Round_1_User_ID
                        GPA_Rounds(1, 0) = dtGPA.Rows(0).Item(3).ToString ' GPA_Round_2
                        GPA_Rounds(1, 1) = dtGPA.Rows(0).Item(4).ToString ' GPA_Round_2_User_ID
                        GPA_Rounds(2, 0) = dtGPA.Rows(0).Item(5).ToString ' GPA_Round_3
                        GPA_Rounds(2, 1) = dtGPA.Rows(0).Item(6).ToString ' GPA_Round_3_User_ID
                        GPA_Rounds(3, 0) = dtGPA.Rows(0).Item(7).ToString ' GPA_Round_4
                        GPA_Rounds(3, 1) = dtGPA.Rows(0).Item(8).ToString ' GPA_Round_4_User_ID
                        GPA_Rounds(4, 0) = dtGPA.Rows(0).Item(9).ToString ' GPA_Round_5
                        GPA_Rounds(4, 1) = dtGPA.Rows(0).Item(10).ToString ' GPA_Round_5_User_ID

                        'Determine what GPA round we are on
                        For i As Integer = 1 To 4
                            If GPA_Rounds(i, 0) = Nothing Then
                                GPA_Round = i + 1
                                Exit For
                            End If
                        Next
                    End If
                    'MessageBox.Show(GPA_Round)
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                End Try



                Image_Viewer.Navigate(New Uri(Image_Full_Path))

                'Hide image on PDFViewer
                Image_Viewer.Visibility = Windows.Visibility.Visible
            End If
        End Sub

        Private Sub btnGetTranscripts_Click(sender As System.Object, e As System.Windows.RoutedEventArgs)
            btnGetTranscripts.IsEnabled = False

            dgImages.Columns.Clear()

            Dim TA_Transcripts As New dsTranscriptsTableAdapters.EM_WD_GetImages_TranscriptsTableAdapter
            Dim dtTranscripts As dsTranscripts.EM_WD_GetImages_TranscriptsDataTable
            dtTranscripts = TA_Transcripts.GetData_Transcripts(EM_WD_Users_ID, EM_WD_Team_ID)

            dtImages = dtTranscripts
            dgImages.ItemsSource = dtImages

            If dtTranscripts.Rows.Count > 0 Then
                dtImages = dtTranscripts
                dgImages.ItemsSource = dtImages

                'Set the viewer to a blank image
                Image_Viewer.Navigate("about:blank")

                'Hide the columns not needed for the view
                'dgImages.Columns(0).Visibility = Windows.Visibility.Hidden 'Image_ID
                dgImages.Columns(1).Visibility = Windows.Visibility.Hidden 'Image_Filename
                dgImages.Columns(2).Visibility = Windows.Visibility.Hidden 'Image_Full_Path
                dgImages.Columns(3).Visibility = Windows.Visibility.Hidden 'Banner_ID

            Else
                If Not dtImages Is Nothing Then
                    dtImages.Clear()
                End If
                MessageBox.Show("There are no images to process.")
            End If
            btnGetTranscripts.IsEnabled = True
        End Sub

        Private Sub Submit_CanExecute(sender As Object, e As CanExecuteRoutedEventArgs)
            If IsLoaded Then
                If dgImages.SelectedIndex < 0 Then
                    'If txtMPG.Text = "" And txtMPG.Visibility = Windows.Visibility.Hidden And _noOfErrorsOnScreen = 1 Then
                    e.CanExecute = False
                    e.Handled = True
                Else
                    e.CanExecute = _noOfErrorsOnScreen = 0
                    e.Handled = True
                End If
            End If
        End Sub

        Private Sub GetTranscripts_CanExecute(sender As Object, e As CanExecuteRoutedEventArgs)
            If IsLoaded Then
                e.CanExecute = True
                e.Handled = True
            End If
        End Sub

        Private Sub Search_CanExecute(sender As Object, e As CanExecuteRoutedEventArgs)
            If IsLoaded Then
                If txtImage_ID_Banner_ID.Text = "" Then
                    e.CanExecute = False
                    e.Handled = True
                Else
                    e.CanExecute = True
                    e.Handled = True
                End If

            End If
        End Sub

        Private Sub btnSearch_Click(sender As System.Object, e As System.Windows.RoutedEventArgs)
            btnGetTranscripts.IsEnabled = False

            dgImages.Columns.Clear()

            Dim TA_Transcripts_by_ID As New dsTranscripts_by_IDTableAdapters.EM_WD_GetImages_Transcripts_By_IDTableAdapter
            Dim dtTranscripts_by_ID As dsTranscripts_by_ID.EM_WD_GetImages_Transcripts_By_IDDataTable
            dtTranscripts_by_ID = TA_Transcripts_by_ID.GetData_Transcripts_by_ID(txtImage_ID_Banner_ID.Text)

            dtImages = dtTranscripts_by_ID
            dgImages.ItemsSource = dtImages

            If dtTranscripts_by_ID.Rows.Count > 0 Then
                dtImages = dtTranscripts_by_ID
                dgImages.ItemsSource = dtImages

                'Set the viewer to a blank image
                Image_Viewer.Navigate("about:blank")


                'Hide the columns not needed for the view
                'dgImages.Columns(0).Visibility = Windows.Visibility.Hidden 'Image_ID
                dgImages.Columns(1).Visibility = Windows.Visibility.Hidden 'Image_Filename
                dgImages.Columns(2).Visibility = Windows.Visibility.Hidden 'Image_Full_Path
                dgImages.Columns(3).Visibility = Windows.Visibility.Hidden 'Banner ID

            Else
                If Not dtImages Is Nothing Then
                    dtImages.Clear()
                End If
                MessageBox.Show("There are no transcripts with that Banner or Image ID.")
            End If
            btnGetTranscripts.IsEnabled = True
        End Sub

        Private Sub Load_GPA_Conversion()
            Try
                Dim GPA_ConversionTableAdapter As New dsGPA_ConversionTableAdapters.EM_WD_GetGPA_ConversionTableAdapter
                dtGPA_Conversion = GPA_ConversionTableAdapter.GetData_GPA_Conversion()
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Private Sub txtGrade_Lost_Focus(ByVal sender As System.Object, ByVal e As System.EventArgs)
            If Not AreAllValidNumericChars(sender.text) And Not LetterGradeCheck(sender.text) Then
                MessageBox.Show("The grade is in the incorrect format")
                sender.text = ""
            ElseIf sender.text <> "" And Not AreAllValidNumericChars(sender.text) Then
                'Convert the grade and update the label
                'Need to get the row number and then change the text for Recalc_Grade label to reflect the converted grade.
                dtGPA_Conversion.DefaultView.RowFilter = "EM_WD_GPA_Letter_Grade = '" & sender.text & "'"

                'If no rows are found, send error
                If dtGPA_Conversion.DefaultView.Count = 0 Then
                    MessageBox.Show("The grade was not found in the conversion table.")
                    sender.text = ""
                End If
            End If


            'If TryCast(cbxGrade_Type, ComboBox).SelectedItem.Content = "Numeric" Then
            '    'Check to make sure grade is numeric
            '    If Not AreAllValidNumericChars(sender.text) Then
            '        MessageBox.Show("The grade is in the incorrect format")
            '        sender.text = ""
            '    End If
            'Else
            '    'Check to make sure grade is a letter
            '    If Not LetterGradeCheck(sender.text) Then
            '        MessageBox.Show("The grade is in the incorrect format")
            '        sender.text = ""
            '    ElseIf sender.text <> "" Then
            '        'Convert the grade and update the label
            '        'Need to get the row number and then change the text for Recalc_Grade label to reflect the converted grade.
            '        dtGPA_Conversion.DefaultView.RowFilter = "EM_WD_GPA_Letter_Grade = '" & sender.text & "'"

            '        'If no rows are found, send error
            '        If dtGPA_Conversion.DefaultView.Count = 0 Then
            '            MessageBox.Show("The grade was not found in the conversion table.")
            '            sender.text = ""
            '        End If
            '    End If
            'End If
            'If sender.text <> "" Then
            'Disable the option to select a new grade type
            'cbxGrade_Type.IsEnabled = False
            'End If
        End Sub

        Function Recalc_Grade(ByVal Grade As String)
            Dim New_Grade As Decimal = 0
            If AreAllValidNumericChars(Grade) Then
                'If TryCast(cbxGrade_Type, ComboBox).SelectedItem.Content = "Numeric" Then
                'Check to make sure grade is numeric
                If Grade <> "" Then
                    'Convert the grade 
                    Dim MPG As Decimal = txtMPG.Text 'Minimum Passing Grade
                    Dim AG As Decimal = Grade 'Actual Grade
                    Dim GPA As Decimal = 0 'Converted GPA

                    'If the actual grade is less than the minimum passing grade the grade should be zero.
                    If AG < MPG Then
                        New_Grade = 0
                    Else
                        GPA = 4 - (((((MPG - 60) * 0.300001) + 93) - AG) * 0.1)
                        ''Old formula set to 4.3 scale
                        'If AG > 96.9 Then
                        '    GPA = 4.3 - (100 - AG) * 0.0707
                        'ElseIf AG > 93 And MPG < 67 Then
                        '    GPA = 4 + (AG - 94) * 0.0707
                        'Else
                        '    GPA = 4 - (((((MPG - 60) * 0.300001) + 93) - AG) * 0.1)
                        'End If
                    End If
                    'Send the converted GPA to the label
                    New_Grade = GPA
                End If
            ElseIf LetterGradeCheck(Grade) Then
                'Check to make sure grade is a letter
                If Grade <> "" Then
                    'Convert the grade and update the label
                    'Need to get the row number and then change the text for Recalc_Grade label to reflect the converted grade.
                    dtGPA_Conversion.DefaultView.RowFilter = "EM_WD_GPA_Letter_Grade = '" & Grade & "'"

                    New_Grade = dtGPA_Conversion.DefaultView.Item(0).Item("EM_WD_GPA_Converted_Grade")
                End If
            Else
                'If the grade is in the wrong format return nothing
                New_Grade = Nothing
            End If

            Return New_Grade
        End Function

        Sub Setup_User_ID()
            Try
                'Setup the User ID
                Dim dtUsers_ID As DataTable
                Dim TA_Users_ID As New dsUser_IDTableAdapters.EM_WD_GetUsers_by_UsernameTableAdapter
                dtUsers_ID = TA_Users_ID.GetData_User_ID(Environment.UserName)
                If dtUsers_ID.Rows.Count = 0 Then
                    'There are no users with that ID.  
                    'Disable submit
                    btnSubmit.IsEnabled = False

                    MessageBox.Show("Please contact your supervisor to have your ID setup in the system.")
                Else
                    EM_WD_Users_ID = dtUsers_ID.Rows(0).Item(0).ToString
                    EM_WD_Team_ID = dtUsers_ID.Rows(0).Item(1).ToString

                    'EM_WD_Users_ID = 6
                    'EM_WD_Team_ID = 3

                    'EM_WD_Users_ID = 6
                    'EM_WD_Team_ID = 3

                    'Enable submit
                    btnSubmit.IsEnabled = True
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End Sub

        Protected Overrides Sub OnPreviewTextInput(e As System.Windows.Input.TextCompositionEventArgs)
            If InStr(e.Source.ToString, "Textbox", CompareMethod.Text) Then
                Dim txtBox_Name As String = TryCast(e.Source, TextBox).Name

                If InStr(txtBox_Name, "Credit", CompareMethod.Text) Then
                    'Check to make sure grade is numeric
                    e.Handled = Not AreAllValidNumericChars(e.Text)
                ElseIf InStr(txtBox_Name, "Banner_ID", CompareMethod.Text) Then
                    'Check to make sure grade is numeric
                    e.Handled = Not AreAllValidNumericChars(e.Text)
                ElseIf InStr(txtBox_Name, "txtMPG", CompareMethod.Text) Then
                    txtMPG.ClearValue(TextBox.BorderBrushProperty)
                    txtMPG.ClearValue(TextBox.BorderThicknessProperty)
                    e.Handled = Not AreAllValidNumericChars(e.Text)
                ElseIf InStr(txtBox_Name, "Grade", CompareMethod.Text) Then

                    e.Handled = Not AreAllValidNumericChars(e.Source.Text & e.Text) And Not LetterGradeCheck(e.Source.Text & e.Text)
                    'If TryCast(cbxGrade_Type, ComboBox).SelectedItem.Content = "Numeric" Then
                    '    'Check to make sure grade is numeric
                    '    e.Handled = Not AreAllValidNumericChars(e.Source.Text & e.Text)
                    'Else
                    '    'Check to make sure grade is a letter
                    '    e.Handled = Not LetterGradeCheck(e.Source.Text & e.Text)
                    'End If
                End If

                MyBase.OnPreviewTextInput(e)
            End If
        End Sub

        Sub ResetForm()

            For i As Int32 = 0 To Grid1.Children.Count - 1
                If Grid1.Children.Item(i).GetType.FullName = GetType(TextBox).FullName Then
                    Dim ctrl As TextBox = CType(Grid1.Children.Item(i), TextBox)
                    'now you have this control. change textblock to whatever type you are looking for.
                    ctrl.Text = ""
                ElseIf Grid1.Children.Item(i).GetType.FullName = GetType(RadioButton).FullName Then
                    Dim ctrl As RadioButton = CType(Grid1.Children.Item(i), RadioButton)
                    'now you have this control. change textblock to whatever type you are looking for.
                    If InStr(ctrl.Name, "CP", CompareMethod.Text) Then
                        ctrl.IsChecked = True
                    Else
                        ctrl.IsChecked = False
                    End If
                End If
            Next i

            txtCEEB.Text = ""
            txtMPG.Text = ""

            rdl_Status_Standard.IsChecked = True

            'cbxGrade_Type.SelectedItem = cbxGrade_Type.Items(0)
            Grid1.Children.Item(0).Focus()

            'Hide image on PDFViewer
            Image_Viewer.Visibility = Windows.Visibility.Hidden
            'Set the viewer to a blank image
            Image_Viewer.Navigate("about:blank")
        End Sub

        Private Function AreAllValidNumericChars(str As String) As Boolean
            For Each c As Char In str
                If Not [Char].IsNumber(c) Then
                    If Not str = "." Then
                        Return False
                    End If
                End If
            Next
            Return True
        End Function

        Function LetterGradeCheck(ByVal LetterGrade As String) As Boolean

            Dim bStatus As Boolean = True

            If Not LetterGrade = "" Then

                Dim pattern As String = "^([a-fA-F])([/+/-])?$"

                Dim LetterGradeMatch As Match = Regex.Match(LetterGrade, pattern)
                If Not LetterGradeMatch.Success Then
                    bStatus = False
                End If

            End If
            Return bStatus

        End Function

        Private Sub btnSubmit_Click(sender As System.Object, e As System.Windows.RoutedEventArgs)

            Dim intLoopIndex As Integer
            Dim index As Integer = 0
            Dim SumofWeightedGPAs As Decimal = 0
            Dim SumofUnweightedGPAs As Decimal = 0
            Dim SumofCourseCredits As Decimal = 0
            Dim CountofGrades As Integer = 0

            Dim FieldError As Boolean = False

            Dim Image_ID As Integer = 0

            Used_GPA_Rounds_User_IDs = EM_WD_Users_ID
            'GPA_Passed_QC = False
            GPA_Passed_QC = True '//ds 9/15/14 - for taking out double blind check

            If dgImages.SelectedIndex > -1 Then
                Image_ID = DirectCast(dgImages.SelectedItem, DataRowView).Row("Image_ID").ToString()
            End If

            For intLoopIndex = 1 To 100
                Dim txtGrade As TextBox = CType(Grid1.Children.Item(index), TextBox)
                index = index + 1
                Dim rdlCP As RadioButton = CType(Grid1.Children.Item(index), RadioButton)
                index = index + 1
                Dim rdlHon As RadioButton = CType(Grid1.Children.Item(index), RadioButton)
                index = index + 1
                Dim rdlAP As RadioButton = CType(Grid1.Children.Item(index), RadioButton)
                index = index + 1
                Dim txtCredits As TextBox = CType(Grid1.Children.Item(index), TextBox)
                index = index + 2

                'Dim test As Boolean = AreAllValidNumericChars(txtGrade.Text)
                'Dim test2 As String = txtMPG.Text

                If txtGrade.Text <> "" And txtCredits.Text <> "" And txtCEEB.Text <> "" Then
                    If AreAllValidNumericChars(txtGrade.Text) Then
                        'There must be a minimum passing grade if not there is an error.
                        If txtMPG.Text = "" Then
                            'Show error
                            FieldError = True
                            txtMPG.BorderBrush = New System.Windows.Media.SolidColorBrush(Colors.Red)
                            txtMPG.BorderThickness = New System.Windows.Thickness(2)
                            MessageBox.Show("The minimum passing grade is required.")
                            GoTo SkipProcessing_Error_Found
                        Else
                            'Reset the textbox back to normal
                            txtMPG.ClearValue(TextBox.BorderBrushProperty)
                            txtMPG.ClearValue(TextBox.BorderThicknessProperty)
                        End If
                    End If

                    'Determine the course level
                    Dim CourseLevel As Decimal = 0
                    If rdlHon.IsChecked Then
                        CourseLevel = 0.1
                    ElseIf rdlAP.IsChecked Then
                        CourseLevel = 0.2
                    End If

                    Dim WeightedCourseGPA As Decimal
                    Dim UnweightedCourseGPA As Decimal

                    'Add the course level to the recalculated GPA
                    WeightedCourseGPA = Recalc_Grade(txtGrade.Text) + CourseLevel

                    'Add the course level to the recalculated GPA
                    UnweightedCourseGPA = Recalc_Grade(txtGrade.Text)

                    'Add current weighted GPA to previous calculated GPAs
                    SumofWeightedGPAs = SumofWeightedGPAs + (WeightedCourseGPA * txtCredits.Text)

                    'Add current weighted GPA to previous calculated GPAs
                    SumofUnweightedGPAs = SumofUnweightedGPAs + (UnweightedCourseGPA * txtCredits.Text)

                    'Add current course credit to previous credits
                    SumofCourseCredits = SumofCourseCredits + txtCredits.Text

                    'Count how many GPAs have been calcuated to get average
                    CountofGrades = CountofGrades + 1

                    Dim MPG As String = txtMPG.Text

                    If Trim(MPG) = "" Then
                        MPG = Nothing
                    End If

                    Try
                        'Load the row into the database
                        Dim TA_Submit_Grades As New Submit_GradesTableAdapters.QueriesTableAdapter
                        TA_Submit_Grades.EM_WD_Submit_Image_Transcript_Grades_ins(Nothing, Image_ID, txtCEEB.Text, Nothing, MPG, intLoopIndex, txtGrade.Text, txtCredits.Text, CourseLevel, UnweightedCourseGPA, WeightedCourseGPA, EM_WD_Users_ID)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString)
                    End Try

                    'Reset the textbox back to normal
                    txtCredits.ClearValue(TextBox.BorderBrushProperty)
                    txtCredits.ClearValue(TextBox.BorderThicknessProperty)
                Else
                    'If a grade is entered then credits must also be entered
                    If txtGrade.Text <> "" Then
                        If txtCredits.Text = "" Then
                            txtCredits.BorderBrush = New System.Windows.Media.SolidColorBrush(Colors.Red)
                            txtCredits.BorderThickness = New System.Windows.Thickness(2)
                            MessageBox.Show("Credits are missing for one of the grades")
                            FieldError = True
                        End If
                    End If
                End If
            Next

SkipProcessing_Error_Found:
            'Only process if the Standard GPA radio button is checked
            If (CountofGrades > 0 And rdl_Status_Standard.IsChecked And FieldError = False) Or rdl_Status_ADM.IsChecked Then
                Dim FinalUnweightedGPA As Decimal = 0
                Dim FinalWeightedGPA As Decimal = 0

                'Can't divide by zero
                If SumofCourseCredits > 0 Then
                    FinalWeightedGPA = SumofWeightedGPAs / SumofCourseCredits
                    FinalUnweightedGPA = SumofUnweightedGPAs / SumofCourseCredits
                End If


                'GPA can not be higher than 4.0
                '//ds 8/25/14 - commented out as requested. no longer capping GPA at 4.00
                'If FinalWeightedGPA > 4.0 Then
                '    FinalWeightedGPA = 4.0
                'End If

                ''GPA can not be higher than 4.0
                'If FinalUnweightedGPA > 4.0 Then
                '    FinalUnweightedGPA = 4.0
                'End If

                'GPA can not be higher than 4.3
                'If FinalWeightedGPA > 4.3 Then
                '    FinalWeightedGPA = 4.3
                'End If

                ''GPA can not be higher than 4.3
                'If FinalUnweightedGPA > 4.3 Then
                '    FinalUnweightedGPA = 4.3
                'End If

                FinalWeightedGPA = Math.Round(FinalWeightedGPA, 2)
                FinalUnweightedGPA = Math.Round(FinalUnweightedGPA, 2)


                'If the rdl_Status_ADM.IsChecked load ADM and the comment

                Dim GPA_Value_To_Load As String
                Dim GPA_Comment As String

                If rdl_Status_ADM.IsChecked Then
                    GPA_Value_To_Load = "ADM"
                    GPA_Comment = cbx_Status_ADM.Text
                Else
                    GPA_Value_To_Load = FinalWeightedGPA.ToString("#,##0.00")
                    GPA_Comment = Nothing
                End If

                'Compare FinalWeightedGPA to all previous GPAs entered
                'GPA_Round of 1 means no GPAS have been calculated for this transcript or Image_ID is not used
                '//ds 8/25/14 - commented out as requested. Dble Blind keying will no longer be used
                'If GPA_Round > 1 Then
                '    'Compare to check 1
                '    'If GPA is equal to round 1 then remove it from the queues
                '    If GPA_Round = 2 And InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) Then
                '        'There is no problem with the GPA passed QC
                '        GPA_Passed_QC = True
                '        GoTo GPA_Passed
                '    ElseIf GPA_Round = 2 And InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) = False Then
                '        'There is a problem with the GPA and it needs to go on to another round
                '        'We need to not assign it to the same user as the first round
                '        Used_GPA_Rounds_User_IDs = Used_GPA_Rounds_User_IDs & "," & GPA_Rounds(0, 1)
                '        GPA_Passed_QC = False
                '    ElseIf GPA_Round = 3 And (InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(1, 0))) Then
                '        'If the GPA matches the first person or second person, the GPA passes QC
                '        GPA_Passed_QC = True
                '        GoTo GPA_Passed
                '    ElseIf GPA_Round = 3 And InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(1, 0)) = False Then
                '        'There is a problem with the GPA and it needs to go on to another round
                '        'We need to not assign it to the same user as the first and second rounds
                '        'Assign to QC Problem User
                '        Used_GPA_Rounds_User_IDs = Used_GPA_Rounds_User_IDs & "," & GPA_Rounds(0, 1) & ", " & GPA_Rounds(0, 2)
                '        GPA_Passed_QC = False
                '    ElseIf GPA_Round = 4 And (InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(1, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(2, 0))) Then
                '        'If the GPA matches the first person or second person or third, the GPA passes QC
                '        GPA_Passed_QC = True
                '        GoTo GPA_Passed
                '    ElseIf GPA_Round = 4 And InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(1, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(2, 0)) = False Then
                '        'There is a problem with the GPA and it needs to go on to another round
                '        'We need to not assign it to the same user as the first, second and third rounds
                '        'Assign to QC Problem User
                '        Used_GPA_Rounds_User_IDs = Used_GPA_Rounds_User_IDs & "," & GPA_Rounds(0, 1) & ", " & GPA_Rounds(0, 2) & ", " & GPA_Rounds(0, 3)
                '        GPA_Passed_QC = False
                '    ElseIf GPA_Round = 5 And (InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(1, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(2, 0)) Or InRange(GPA_Value_To_Load, GPA_Rounds(3, 0))) Then
                '        'If the GPA matches the first person or second person or third or fourth, the GPA passes QC
                '        GPA_Passed_QC = True
                '        GoTo GPA_Passed
                '    ElseIf GPA_Round = 5 And InRange(GPA_Value_To_Load, GPA_Rounds(0, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(1, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(2, 0)) = False And InRange(GPA_Value_To_Load, GPA_Rounds(3, 0)) = False Then
                '        'GPA does not go on to another round
                '        Used_GPA_Rounds_User_IDs = Nothing
                '        GPA_Passed_QC = False
                '    End If
                'End If
GPA_Passed:
                'Load GPA into database.  Also include the current round = GPA_Round

                Try
                    Dim TA_Submit_GPA As New Submit_GPATableAdapters.QueriesTableAdapter
                    TA_Submit_GPA.EM_WD_Submit_Image_Transcript_GPA_ins(Image_ID, FinalUnweightedGPA.ToString("#,##0.00"), FinalWeightedGPA.ToString("#,##0.00"), CountofGrades, SumofCourseCredits, EM_WD_Users_ID, GPA_Round, GPA_Passed_QC, GPA_Value_To_Load, GPA_Comment)
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                End Try

                'If GPA does not pass QC, assign it to another person
                'If GPA_Round => 3 then assign to QC Problem User
                If GPA_Passed_QC = False Then

                    'If GPA_Round => 3 or < 6 then assign to QC Problem User
                    If GPA_Round > 2 And GPA_Round < 6 Then
                        Try
                            Dim TA_Submit_Image_Queues_Problem As New Submit_Image_Queues_ProblemTableAdapters.QueriesTableAdapter
                            TA_Submit_Image_Queues_Problem.EM_WD_AssignNextGPARound_Problem(Image_ID, EM_WD_Users_ID, 93)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString)
                        End Try
                        'ElseIf GPA_Round < 3 Then
                        '    'If another QC round is needed add/update Image_Queues
                        '    Dim TA_Submit_Image_Queues As New Submit_Image_QueuesTableAdapters.EM_WD_AssignNextGPARoundTableAdapter
                        '    Dim dtSubmit_Image_Queues As Submit_Image_Queues.EM_WD_AssignNextGPARoundDataTable
                        '    dtSubmit_Image_Queues = TA_Submit_Image_Queues.GetData_Submit_Image_Queues(Image_ID, EM_WD_Users_ID)

                        '    If dtSubmit_Image_Queues.Rows(0).Item("Update_Queue").ToString() = "False" Then
                        '        'The transcript was unable to assign to anyone else
                        '        MessageBox.Show("The transcript was unable to assign to anyone else for the next QC check. Image ID = " & Image_ID)
                        '    End If
                    End If
                Else

                    'Check to see if the ADM code is being loaded.  If it is then mark the previous file as the QC Pass
                    If GPA_Value_To_Load = "ADM" Then
                        Try
                            'Need to mark the record as GPA_Approved_by_QC_Range
                            Dim TA_Submit_QC_Range As New Submit_QC_RangeTableAdapters.QueriesTableAdapter
                            TA_Submit_QC_Range.EM_WD_Submit_Image_Transcript_GPA_QC_updt(Image_ID, 0, 0, "ADM")
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString)
                        End Try
                    Else
                        'PASSED QC - Determine the correct GPA for loading into Banner
                        Dim Min As Decimal
                        Dim Max As Decimal

                        If FinalWeightedGPA >= 2.9499999 AndAlso FinalWeightedGPA <= 2.99999 Then
                            Min = FinalWeightedGPA
                            Max = FinalWeightedGPA
                        Else
                            Min = FinalWeightedGPA - 0.05
                            Max = FinalWeightedGPA + 0.05
                        End If
                        Try
                            'Load GPA into database.  Also include the current round = GPA_Round
                            Dim TA_Submit_QC_Range As New Submit_QC_RangeTableAdapters.QueriesTableAdapter
                            TA_Submit_QC_Range.EM_WD_Submit_Image_Transcript_GPA_QC_updt(Image_ID, Min, Max, Nothing)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString)
                        End Try


                    End If

                End If
                'Show the GPA to the user if they did not select ADM
                If GPA_Value_To_Load <> "ADM" Then
                    MessageBox.Show(FinalWeightedGPA.ToString("#,##0.00"))
                End If


                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.SelectedIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()
                Else
                    'Update the datagrid
                    dgImages.ItemsSource = dtImages
                End If

                ResetForm()
            ElseIf rdl_Status_Unable.IsChecked Then
                'The GPA can not be calculated, add OPS code, comment and remove from queue
                Dim TA_Submit_GPA As New Submit_GPATableAdapters.QueriesTableAdapter
                TA_Submit_GPA.EM_WD_Submit_Image_Transcript_GPA_ins(Image_ID, 0, 0, 0, 0, EM_WD_Users_ID, GPA_Round, 1, "OPS", cbx_Status_Unable.Text)

                'Need to mark the record as GPA_Approved_by_QC_Range
                Dim TA_Submit_QC_Range As New Submit_QC_RangeTableAdapters.QueriesTableAdapter
                TA_Submit_QC_Range.EM_WD_Submit_Image_Transcript_GPA_QC_updt(Image_ID, 0, 0, "OPS")

                'Remove the selected row from the datagrid
                Dim theRowToDelete As DataRow = dtImages.Rows(dgImages.SelectedIndex)
                dtImages.Rows.Remove(theRowToDelete)

                'Check to see if there are any remaining rows.  If not clear the datagrid.
                If dtImages.Rows.Count = 0 Then
                    dgImages.Columns.Clear()
                Else
                    'Update the datagrid
                    dgImages.ItemsSource = dtImages
                End If

                ResetForm()
            Else
                'No GPAs could be calculated
                MessageBox.Show("There is an error with one of the fields or all fileds are blank")
            End If


            'MessageBox.Show("Form submitted and reset")
        End Sub

        Public Shared Function InRange(ByVal FinalGPA As String, ByVal PreviousGPA As String) As Boolean

            Dim GPA_Range As Boolean = False

            'Check to see if incoming number is numeric.  If not then compare strings.  If they are compare decimals
            If IsNumeric(FinalGPA) = False Or IsNumeric(PreviousGPA) = False Then
                If FinalGPA = PreviousGPA Then
                    GPA_Range = True
                End If
            Else
                If FinalGPA >= 2.9499999 AndAlso FinalGPA <= 2.99999 Then
                    If FinalGPA = PreviousGPA Then
                        GPA_Range = True
                    End If
                Else
                    Dim Min As Decimal = PreviousGPA - 0.05
                    Dim Max As Decimal = PreviousGPA + 0.05
                    If FinalGPA > Min AndAlso FinalGPA < Max Then
                        GPA_Range = True
                    End If
                End If
            End If

            Return GPA_Range
        End Function

        Public Shared Function InRange_Old(ByVal FinalGPA As Decimal, ByVal PreviousGPA As Decimal) As Boolean
            If FinalGPA >= 2.9499999 AndAlso FinalGPA <= 2.99999 Then
                If FinalGPA = PreviousGPA Then
                    Return True
                Else
                    Return False
                End If
            Else
                Dim Min As Decimal = PreviousGPA - 0.05
                Dim Max As Decimal = PreviousGPA + 0.05
                If FinalGPA > Min AndAlso FinalGPA < Max Then
                    Return True
                Else
                    Return False
                End If

            End If
        End Function

    End Class
End Namespace
