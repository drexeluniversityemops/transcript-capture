﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.ComponentModel
Imports System.Text.RegularExpressions

Namespace Transcript_Capture

    Public Class clsValidation
        Implements IDataErrorInfo

        Public Property Image_ID_Banner_ID() As String
            Get
                Return m_Image_ID_Banner_ID
            End Get
            Set(value As String)
                m_Image_ID_Banner_ID = value
            End Set
        End Property
        Private m_Image_ID_Banner_ID As String

        Public Property CEEB() As String
            Get
                Return m_CEEB
            End Get
            Set(value As String)
                m_CEEB = value
            End Set
        End Property
        Private m_CEEB As String

        Public Property MPG() As String
            Get
                Return m_MPG
            End Get
            Set(value As String)
                m_MPG = value
            End Set
        End Property
        Private m_MPG As String

#Region "IDataErrorInfo Members"

        Public ReadOnly Property [Error]() As String Implements IDataErrorInfo.[Error]
            Get
                Throw New NotImplementedException()
            End Get
        End Property

        Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
            Get
                Dim result As String = Nothing
                If columnName = "Image_ID_Banner_ID" Then
                    Dim number As Integer
                    If Not Integer.TryParse(DirectCast(Image_ID_Banner_ID, String), number) Then
                        result = "Invalid number format"
                    End If
                    If number < 10000 OrElse number > 2000000 And number < 10000000 OrElse number > 69999999 Then
                        result = "Invalid Image ID"
                    End If
                End If
                If columnName = "CEEB" Then
                    If DirectCast(CEEB, String) = "" Then
                        result = "CEEB is required"
                    End If
                End If
                If columnName = "MPG" Then
                    Dim number As Decimal
                    If MPG <> "" Then
                        If Not Decimal.TryParse(DirectCast(MPG, String), number) Then
                            result = "Invalid number format"
                            'result = ""
                        End If
                    End If
                End If
                Return result
            End Get
        End Property

#End Region
    End Class

End Namespace